<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRateExchangeTable extends Migration
{

    public $timestamps = false;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rate_exchange', function (Blueprint $table) {
            $table->bigIncrements('id')->nullable(false);
            $table->integer('id_user')->nullable(false);
            $table->integer('id_local_currency')->nullable(false);
            $table->integer('id_foreign_currency')->nullable(false);
            $table->boolean('active')->nullable(false)->default(true);
            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_local_currency')->references('id')->on('currencies')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_foreign_currency')->references('id')->on('currencies')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rate_exchange');
    }
}

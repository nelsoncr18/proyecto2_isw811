<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{

    public $timestamps = false;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id')->nullable(false);
            $table->integer('id_user')->nullable(false);
            $table->integer('id_account')->nullable(false);
            $table->integer('id_parent')->nullable(true);
            $table->integer('id_type')->nullable(false);
            $table->text('name')->nullable(false);
            $table->text('description')->nullable(false);
            $table->boolean('active')->nullable(false)->default(true);
            $table->foreign('id_user')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_account')->references('id')->on('accounts')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_parent')->references('id')->on('categories')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_type')->references('id')->on('category_types')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}

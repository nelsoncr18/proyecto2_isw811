<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{

    public $timestamps = false;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id')->nullable(false);
            $table->integer('id_type')->nullable(false);
            $table->integer('id_account')->nullable(false);
            $table->integer('id_category')->nullable(true);
            $table->timestamp('date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->text('detail')->nullable(true);
            $table->float('amount')->nullable(false);
            $table->boolean('active')->nullable(false)->default(true);
            $table->foreign('id_category')->references('id')->on('categories')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_account')->references('id')->on('accounts')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_type')->references('id')->on('transaction_types')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
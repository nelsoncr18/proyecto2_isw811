<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountsTable extends Migration
{

    public $timestamps = false;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->bigIncrements('id')->nullable(false);
            $table->integer('id_owner')->nullable(false);
            $table->text('name')->nullable(false);
            $table->text('description')->nullable(true);
            $table->integer('id_currency')->nullable(false);
            $table->integer('initial_balance')->nullable(false)->default(0);
            $table->boolean('active')->nullable(false)->default(true);
            $table->foreign('id_owner')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('id_currency')->references('id')->on('currencies')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::get('/login', function () {
    return view('auth/login');
});

Route::get('/register', function () {
    return view('auth/register');
});

Auth::routes();

Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/settings', 'UserController@index')->name('settings')->middleware('auth');
Route::get('/categories', 'CategoryController@index')->name('categories')->middleware('auth');
Route::get('/shareds', 'HomeController@shareds')->name('shareds')->middleware('auth');


Route::get('/reports/lastmonth', 'ReportsController@lastmonth')->name('LM')->middleware('auth');
Route::get('/reports/between', 'ReportsController@between_two_dates')->name('BTD')->middleware('auth');
Route::get('/reports/last_year', 'ReportsController@lastyear')->name('LY')->middleware('auth');

Route::post('/reports/between', 'ReportsController@loadBetween')->name('loadbetween')->middleware('auth');
Route::post('/reports/lastmonth', 'ReportsController@search_by_month')->name('lastmonth')->middleware('auth');
Route::post('/reports/last_year', 'ReportsController@search_by_year')->name('lastyear')->middleware('auth');
Route::post('/transactions', 'TransactionController@store')->middleware('auth');



Route::resource('/allows', 'UserAllowedController')->middleware('auth');
Route::resource('/accounts', 'AccountController')->middleware('auth');
Route::resource('/categories', 'CategoryController')->middleware('auth');
Route::resource('/currencies', 'CurrencyController')->middleware('auth');
Route::resource('/transactions', 'TransactionController')->middleware('auth');
Route::resource('/local_currencies', 'SetCurrenciesController')->middleware('auth');
Route::resource('/graph', 'GraphController')->middleware('auth');


Route::get('/share', function (Request $request) {
    return view('accounts.share');
})->name('share')->middleware('auth');
Route::get('/loading', function () {
return view('layouts.loader');
})->name('loading')->middleware('auth');
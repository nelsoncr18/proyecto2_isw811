@extends('layouts.app')
@section('content')
<header>
    <link href="{{ asset('css/share.css') }}" rel="stylesheet">
</header>

<div class="container">
  <div class="modal-body">
        <form method="POST" action="{{ route('allows.store')}}">
            @csrf
            @method('POST')
            <div class="form-group">
                <label for="formGroupExampleInput">Inserte el correo electrónico del usuario para compartir la cuenta</label>
                <br>
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus autocomplete="off">
                <input type="hidden" name="id" value=" {{ app('request')->input('id') }} ">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-lg btn-primary">Compartir</button>
            <a type="button" href="{{ route('home') }}" class="btn btn-lg btn-danger" data-dismiss="modal">Cancelar</a>
            </div>
        </form>
      </div>
</div>
@endsection
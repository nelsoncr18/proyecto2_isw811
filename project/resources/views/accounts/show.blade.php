@extends('layouts.app')
@section('content')

<header>
    <link href="{{ asset('css/show.css') }}" rel="stylesheet">
</header>

<div class="container">
    <div class="row">
      <div class="col-sm-9">
        <br>
        <h1 class="info text-b">Cuenta: {{ $account->name }}
          <span class="badge
            badge-success">Saldo: {{ $account->initial_balance }}</span></h1>
      </div>
    </div>

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('accounts.show', $account->id) }}">{{ $account->name }}</a></li>
        <li class="breadcrumb-item"><a href="#">Información</a></li>
      </ol>
    </nav>

    <div class="row">
      <div class="col-sm-3">
        <!--Information-->
        <ul class="list-group">
          <li class="list-group-item text-muted"><i class="fa fa-info-circle
              fa-1x"></i>&nbsp;&nbsp;Información</li>
          <li class="list-group-item text-right"><span class="pull-left"><strong>Moneda</strong></span>
            {{ $currency->description }}</li>
          <li class="list-group-item text-right"><span class="pull-left"><strong>Saldo inicial</strong></span> {{ $account->initial_balance }}</li>
        </ul>
        <br>
        <!--Activity-->
        <ul class="list-group">
          <li class="list-group-item text-muted"><i class="fa fa-dashboard
              fa-1x"></i>&nbsp;&nbsp;Actividad</li>
          <li class="list-group-item text-right"><span class="pull-left"><strong>Comparte</strong></span>
            {{ count($shared) }}</li>
          <li class="list-group-item text-right"><span class="pull-left"><strong>Transacciones</strong></span>
            {{ count($transactions) }}</li>
        </ul>
        <br>
        <ul class="list-group">
          <li class="list-group-item text-muted"><i class="fa fa-money
              fa-1x"></i>&nbsp;&nbsp;Moneda</li>
          <li class="list-group-item text-right"><span
              class="pull-left"><strong>Nombre</strong></span>
              {{ $currency->name }}</li>
          <li class="list-group-item text-right"><span
            class="pull-left"><strong>Símbolo</strong></span>
              {{ $currency->symbol }}</li>
        </ul>
        <br>
        <!--Danger Zone-->
        <ul class="list-group">
          <li class="list-group-item text-muted"><i class="fa fa-exclamation-triangle
              fa-1x"></i>&nbsp;&nbsp;Zona peligrosa</li>
          <li class="list-group-item text-muted">
            <button type="button" class="btn btn-danger btn-sm btn-block" data-toggle="modal" data-target="#deleteModal">Eliminar cuenta</button>
          </li>
          <li class="list-group-item text-muted">
            <a type="button" href="{{ route('graph.show', $account->id) }}" class="btn btn-info btn-sm btn-block">Gráficos</a>
          </li>
        </ul>
      </div>

      <!--TabPane-->
      <div class="col-sm-9">
        &nbsp;
        <ul class="nav nav-tabs">
          <li class="nav-item active"><a href="#categories" class="nav-link
              active"
              data-toggle="tab"><i class="fa fa-folder
                fa-1x"></i>&nbsp; Categorias</a></li>
          <li class="nav-item active"><a href="#transactions" class="nav-link"
              data-toggle="tab"><i class="fa fa-exchange
                fa-1x"></i>&nbsp; Transacciones</a></li>
          <li class="nav-item"><a href="#shared-with" class="nav-link"
              data-toggle="tab"><i class="fa fa-user
                fa-1x"></i>&nbsp; Compartir con</a></li>
          <li class="nav-item"><a href="#edit" class="nav-link"
              data-toggle="tab"><i class="fa fa-pencil
                fa-1x"></i>&nbsp; Editar</a></li>
        </ul>

        <div class="tab-content">
          <!--Categories list section-->
          <div class="tab-pane active" id="categories">
            <div class="table-responsive">
              <div>
              <br>
              <button type="button" class="btn btn-success btn-md btn-block" data-toggle="modal" data-target=".bd-example-modal-xl">Nueva categoría</button>
            </div>
              <table class="table table-hover">
                <thead>
                  <tr class="text-b">
                    <th>Nombre</th>
                    <th>Padre</th>
                    <th>Tipo</th>
                    <th>Descripción</th>
                  </tr>
                </thead>
                <tbody id="items">
                   @forelse ($categories as $own)
                      <tr class="cats text-b" onclick="window.location.href='{{ route('categories.show', $own->id,['idac'=>$account->id]) }}';">
                        <td><p>{{ $own->name }}</p></td>
                        <td><p>{{ $own->id_parent }}</p></td>
                        @if($own->id_type == 1)
                          <td><h5><span class="badge badge-success">Ingreso</span></h5></td>
                        @else
                          <td><h5><span class="badge badge-danger">Gasto</span></h5></td>
                        @endif
                        <td><p>{{ $own->description }} </p></td>
                      </tr>
                  @empty
                  <br>
                  <div class="empty">
                      <h4 class="container-title text-b">No hay datos disponibles</h4>
                  </div>            
                  @endforelse
                </tbody>
              </table>
            </div>
            <hr>
            <div class="pagination">
                  {{ $categories->links() }}      
            </div>
          </div>

          <!--Transaction list section-->
          <div class="tab-pane" id="transactions">
            <div>
              <br>
              <a href="{{ route('transactions.create',['idac'=>$account->id]) }}" type="button" class="btn btn-primary btn-md btn-block btn-t">Nueva transacción</a>
            </div>
            <div class="table-responsive">
              <table class="table table-hover">
                <thead>
                  <tr class="text-b">
                    <th>Detalle</th>
                    <th>Tipo</th>
                    <th>Fecha</th>
                    <th>Cantidad</th>
                  </tr>
                </thead>
                <tbody id="items">
                   @forelse ($transactions as $own)
                      <tr class="cats text-b" onclick="window.location.href='{{ route('transactions.show', $own->id,['idac'=>$account->id]) }}';">
                        <td><p class="detail">{{ $own->detail }}</p></td>                        
                        @if($own->id_type == 1)
                          <td><h5><span class="badge badge-success">Ingreso</span></h5></td>
                        @elseif($own->id_type == 2)
                          <td><h5><span class="badge badge-danger">Gasto</span></h5></td>
                        @else
                          <td><h5><span class="badge badge-danger">Transferencia</span></h5></td>
                        @endif
                        <td><p class="date">{{ $own->date }}</p></td>
                        <td><p>{{ $own->amount }} </p></td>
                      </tr>
                  @empty
                  <br>
                  <div class="empty">
                      <h4 class="container-title text-b">No hay datos disponibles</h4>
                  </div>            
                  @endforelse
                </tbody>
              </table>
            </div>
            <hr>
            <div class="pagination">
                  {{ $transactions->links() }}      
            </div>
          </div>

          <!--Shared with list section-->
          <div class="tab-pane" id="shared-with">
            <div>
              <br>
              <form method="POST" action="{{ route('allows.store')}}">
                  @csrf
                  @method('POST')
                  <div class="form-group">
                    <div class="input-group mb-3">
                      <input id="email" type="email" name="email" class="form-control"  value="{{ old('email') }}" required autofocus autocomplete="off" placeholder="Cuenta a la que se va a compartir" aria-label="Recipient's username" aria-describedby="basic-addon2">
                      <input type="hidden" name="id_account" value=" {{ $account->id }} ">
                      <div class="input-group-append">
                        <button type="submit" class="btn btn-outline-primary" type="button">Compartir</button>
                      </div>
                    </div>
                  </div>
              </form>
            </div>
            <hr>
            <div class="pagination">
                  {{ $shared->links() }}      
            </div>
          </div>

          <!--Edit account section-->
          <div class="tab-pane" id="edit">
            <br>
            <form  method="POST" action="{{ route('accounts.update', $account->id) }}">
              @csrf
              @method('PUT')
              <div class="form-group">
                <div class="col-xs-6">
                  <label class="text-b" for="name"><h4>Nombre</h4></label>
                  <input type="text" class="form-control" name="name" id="name"
                    placeholder="Nombre" value="{{ $account->name }}">
                </div>
              </div>

              <div class="form-group">
                <div class="col-xs-6">
                  <label class="text-b" for="description"><h4>Descripción</h4></label>
                  <input type="text" class="form-control" name="description" id="description"
                    placeholder="Descripción" value="{{ $account->description }}">
                </div>
              </div>

              <div class="form-group">
                <div class="col-xs-6">
                  <label class="text-b" for="id_currency"><h4>Moneda</h4></label> <br>
                  <select name="id_currency" id="id_currency" class="form-control">
                    @forelse ($currencies as $val)
                      @if($account->id_currency == $val->id)
                        <option value = '{{ $val->id }}' selected>{{$val->symbol}} {{$val->description}}</option>
                      @else
                        <option value = '{{ $val->id }}'>{{$val->symbol}} {{$val->description}}</option>
                      @endif
                    @empty
                      <option value="">No hay datos disponibles</option>
                    @endforelse
                </select>
                </div>
              </div>

              <div class="form-group">
                <div class="col-xs-6">
                  <label class="text-b" for="initial_balance">
                    <h4>Saldo inicial</h4></label>
                  <input type="number" class="form-control" name="initial_balance" id="initial_balance"
                    placeholder="80000" value="{{ $account->initial_balance }}">
                </div>
              </div>

              <div class="form-group row mb-0">
                <div class="col-md-8 offset-md-2">
                  <button type="submit" class="btn btn-primary btn-md btn-block">Actualizar</button>
                  &nbsp;
                  <a type="submit" class="btn btn-danger btn-md btn-block" href="{{ route('accounts.show',$account->id) }}">Cancelar</a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--Modal to create category-->
<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="container h-75 justify-content-center align-items-center ">
        <br>
        <form method="POST" action="{{ route('categories.store') }}">
            @csrf
            <div class="form-group">
                <label for="parent">Parent: none</label>
            </div>
            <div class="form-group">
                <label for="id_type">Tipo:</label>
                <select name="id_type" id="id_type">
                @foreach($categories_type as $val)
                    <option value = '{{ $val->id }}' >{{$val->description}}</option>
                    @empty($categories_type)
                        <option value="">No hay datos disponibles</option>
                    @endempty
                @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="name">Nombre:</label>
                <input placeholder="Nombre" type="text" class="form-control" name="name" id="name">
            </div>
            <div class="form-group">
                <label for="description">Descripción:</label>
                <textarea cols="30" rows="10" type="text" class="form-control" name="description" id="description" placeholder="Para comprar en la deep web"></textarea>
                <input type="hidden" name="id_account" value="{{ $account->id }}">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-lg btn-info">Crear</button>
            </div>
        </form>
        <br>
      </div>
    </div>
  </div>
</div>

<!--Modal to confirm the deletion of account-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
      <h5 class="modal-title" id="exampleModalLabel">{{ $account->name }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      De verdad quieres eliminar esta cuenta?
      </div>
      <div class="modal-footer">
          <form action="{{ route('accounts.destroy', $account->id) }}" method="POST">
              @csrf
              @method('DELETE')
            <button class="btn btn-success" type="submit">Confirmar</button>
            <button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          </form>
      </div>
    </div>
  </div>
</div>
@endsection
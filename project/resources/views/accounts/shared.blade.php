@extends('layouts.app')
@section('content')

<header>
    <link href="{{ asset('css/home.css') }}" rel="stylesheet">
</header>

<div class="container">    
    <h3 class="container-title">Cuentas compartidas</h3>
    <hr>
    <div class="card-group">
        <div class="row">
            @forelse ($shared as $share)
                <a href="{{ route('accounts.show', $share->id) }}" class="card text-center">
                    <div class="card-header">
                        <h5>{{ $share->name }}</h5>
                    </div>
                    <div class="card-body">
                        <p class="card-text h6">{{ $share->description }}</p>
                    </div>
                    <div class="card-footer text-muted">
                        <h6> 2 days ago </h6>
                    </div>
                </a>
            &nbsp;
            @empty
                <div class="empty">
                    <h4 class="container-title">No hay datos registrados</h4>
                </div>
            @endforelse
        </div>
    </div>
    <div class="pagination">
        {{ $shared->links() }}      
    </div>
</div>
@endsection
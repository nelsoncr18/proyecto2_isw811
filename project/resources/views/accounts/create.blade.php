@extends('layouts.app')
@section('content')

<header>
    <link href="{{ asset('css/create.css') }}" rel="stylesheet">
</header>

<div class="container-fluid h-50 w-50 formSubmitLoad position-fixed">
        <form method="POST" action="{{ route('accounts.store') }}">
            @csrf
            <div class="form-group">
                <label for="name">Nombre de la cuenta:</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="Salvar">
            </div>
            <div class="form-group">
                <label for="initial_balance">Saldo inicial:</label>
                <input type="number" class="form-control" name="initial_balance" id="initial_balance" placeholder="5000">
            </div>
            <div class="form-group">
                <label for="id_currency">Moneda:</label>
                <br>
                <select class="form-control" name="id_currency" id="id_currency">
                @foreach($currencies as $val)
                    <option value = '{{ $val->id }}' >{{$val->symbol}}    {{$val->description}}</option>
                    @empty($currencies)
                        <option value="">No hay datos disponibles</option>
                    @endempty
                @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="description">Descripción:</label>
                <textarea class="form-control" name="description" id="description" cols="30" rows="10"></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-lg btn-info">Crear</button>
                <button type="button" class="btn btn-lg btn-danger"><a href="{{ route('home') }}">Cancelar</a></button>
            </div>
        </form>
    </div>
@endsection

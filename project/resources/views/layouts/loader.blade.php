@extends('layouts.app')
@section('content')
<header>
    <link href="{{ asset('css/loader.css') }}" rel="stylesheet">
</header>

<div id="loader-wrapper">
    <div id="loader"></div>
</div>
<footer>
    <script type="text/javascript" src="{{ asset('js/loader.js') }}" defer></script>
</footer>
@endsection
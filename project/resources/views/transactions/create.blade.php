@extends('layouts.app')
@section('content')
<div class="container">
<h1>{{ $account->name }}</h1>
<div class="row h-75 justify-content-center align-items-center">
    <form method="POST" action="{{ route('transactions.store') }}">
        @csrf
        <div class="form-group">
            <label for="transaction_type">Seleccione el tipo de transacción:</label>
            <select class="form-control" required name="transaction_type" id="transaction_type">
                @forelse ($transactions_type as $type)
                    <option value="{{$type->id}}">{{$type->description}}</option>
                @empty
                    <option value="">No hay datos disponibles</option>
                @endforelse
            </select>
        </div>
        
        <div class="form-group" id="accounts">
            <label id="accountOt"  for="accountO">Seleccione la cuenta:</label>
            <select class="form-control" required name="accountO" id="accountO">
                @forelse ($accounts as $account)
                    <option value="{{$account->id}}">{{$account->name}}</option>
                @empty
                    <option value="">No hay datos disponibles</option>
                @endforelse
            </select>
        </div>
        <div class="form-group" id="categoryI">
            <label id="categoryI"  for="categories">Categoria:</label>
            <select class="form-control" name="categoryI" id="categoryI">
                @forelse ($categoriesI as $categoryI)
                    <option value="{{$categoryI->id}}">{{$categoryI->name}}</option>
                @empty
                    <option value="">No hay datos disponibles</option>
                @endforelse
            </select>
        </div>
        <div class="form-group">
            <label for="amount">Monto:</label>
            <br>
            <input class="form-control" required type="number" name="amount" id="amount">
            <input class="form-control" type="hidden" name="account" id="account" value="{{app('request')->input('idac')}}">
        </div>
        <div class="form-group">
            <label for="description">Descripción:</label>
            <textarea class="form-control" type="text" name="description" id="description" cols="30" rows="10" required></textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-lg btn-info">Crear</button>
            <a class="btn btn-lg btn-danger" href="{{ route('home') }}">Cancelar</a>
        </div>
    </form>
</div>
<footer>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="{{ asset('js/create-transaction.js') }}" defer></script>
</footer>
</div>
@endsection 
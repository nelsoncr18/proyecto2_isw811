@extends('layouts.app')
@section('content')
<div class="container">
  <div class="container-fluid" >
      <div class="row">
          <div class="col-md-4">
          </div>
          <div class="col-md-4">
          </div>
          
              <div class="col-md-4">
              <a href="" class="btn-lg  btn-success" data-toggle="modal" data-target="#editTransaction">Editar</a>
              <a href="" class="btn-lg  btn-danger" data-toggle="modal" data-target="#deleteTransaction">Eliminar</a>
             </div> 
      </div>
  </div>


<!--Modal to edit transactions-->
<div class="modal fade" id="editTransaction" tabindex="-1" role="dialog" aria-labelledby="editTransaction" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">  
        <div class="modal-body">
         <form method="POST" action="{{ route('transactions.update',$transaction->id) }}">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="transaction_type">Seleccione el tipo de transacción:</label>
                <select class="form-control" required name="transaction_type" id="transaction_type">
                    @forelse ($transactions_type as $type)
                        <option value="{{$type->id}}">{{$type->description}}</option>
                    @empty
                        <option value="">No hay datos disponibles</option>
                    @endforelse
                </select>
            </div>
            
             <div class="form-group">
                <label for="accountO">Seleccione la cuenta objetiva:</label>
                <select class="form-control" required name="accountO" id="accountO">
                    @forelse ($accounts as $account)
                        <option value="{{$account->id}}">{{$account->name}}</option>
                    @empty
                        <option value="">No hay datos disponibles</option>
                    @endforelse
                </select>
            </div>
           <!-- <div class="form-group">
                <label for="date">Date:</label>
                <br>
                <input class="form-control" required type="datetime" name="date" id="date">
            </div>  -->
              <div class="form-group">
                <label for="amount">Cantidad:</label>
                <br>
              <input class="form-control" required type="number" name="amount" id="amount" value="{{$transaction->amount}}">
              </div>
            <div class="form-group">
                <label for="formGroupExampleInput">Descripción:</label>
                <textarea class="form-control"  name="description" id="description" cols="30" rows="10">{{$transaction->detail}}</textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-lg btn-info">Crear</button>
                <a class="btn btn-lg btn-danger" data-dismiss="modal">Cancelar</a>
            </div>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>


<!--Modal to delete transactions-->
 <div class="modal fade" id="deleteTransaction" tabindex="-1" role="dialog" aria-labelledby="deleteTransaction" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          
        <h5 class="modal-title" id="exampleModalLabel">{{$transaction->detail}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        ¿De verdad quieres eliminar esta cuenta?
        </div>
        <div class="modal-footer">
            <form action="{{ route('transactions.destroy', $transaction->id) }}" method="POST">
                @csrf
                @method('DELETE')
              <button class="btn btn-success" type="submit">Confirmar</button>
              <button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </form>
        </div>
      </div>
    </div>
  </div>
@endsection
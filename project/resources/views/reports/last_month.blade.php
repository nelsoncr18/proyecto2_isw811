@extends('layouts.app')
@section('content')
<div class="row h-75 justify-content-center align-items-center">
    <form method="POST" action="{{ route('lastmonth') }}">
        @csrf
        <div class="form-group">
                <label for="id_account">Seleccione la cuenta:</label>
                <select class="form-control" required name="id_account" id="id_account">
                    @forelse ($accounts as $account)
                        <option value="{{$account->id}}">{{$account->name}}</option>
                    @empty
                        <option value="">Datos no disponibles</option>
                    @endforelse
                </select>
            </div>
        <div class="form-group">
            <button type="submit" class="btn btn-lg btn-info">Encontrar</button>
            <a class="btn btn-lg btn-danger" href="{{ route('home') }}">Cancelar</a>
        </div>
    </form>
</div>

<div class="row h-75 justify-content-center align-items-center">
    <div class="container">
  <div class="row">
    <div class="col-sm">
      <h1>Ingresos:</h1>
      <br>
      <h3>{{$currency->symbol ?? '' }} {{$totalIncome ?? ''}}</h3>
      <div class="table-responsive">
              <table class="table table-hover">
                <thead>
                  <tr class="text-b">
                    <th>Detalles</th>
                    <th>Tipos</th>
                    <th>Fecha</th>
                    <th>Cantidad</th>
                  </tr>
                </thead>
                <tbody id="items">
                   @forelse ($transactionsI ?? [] as $ownI)
                      <tr class="cats text-b">
                        <td><p class="detail">{{ $ownI->detail }}</p></td>                        
                        @if($ownI->id_type == 1)
                          <td><h5><span class="badge badge-success">Ingreso</span></h5></td>
                        @elseif($ownI->id_type == 2)
                          <td><h5><span class="badge badge-danger">Gasto</span></h5></td>
                        @else
                          <td><h5><span class="badge badge-danger">Transferencia</span></h5></td>
                        @endif
                        <td><p class="date">{{ $ownI->date }}</p></td>
                        <td><p>{{ $ownI->amount }} </p></td>
                      </tr>
                  @empty
                  <br>
                  <div class="empty">
                      <h4 class="container-title text-b">Datos no disponibles</h4>
                  </div>            
                  @endforelse
                </tbody>
              </table>
              <hr>
            </div>
    </div>
    <div class="col-sm">
    </div>
    <div class="col-sm">
      <h1>Gastos:</h1>
      <br>
      <h3>{{$currency->symbol ?? '' }} {{ $totalSpend ?? ''}}</h3>
      <div class="table-responsive">
              <table class="table table-hover">
                <thead>
                  <tr class="text-b">
                    <th>Detalles</th>
                    <th>Tipo</th>
                    <th>Fecha</th>
                    <th>Cantidad</th>
                  </tr>
                </thead>
                <tbody id="items">
                   @forelse ($transactionsS ?? [] as $ownS)
                      <tr class="cats text-b">
                        <td><p class="detail">{{ $ownS->detail }}</p></td>                        
                        @if($ownS->id_type == 1)
                          <td><h5><span class="badge badge-success">Ingreso</span></h5></td>
                        @elseif($ownS->id_type == 2)
                          <td><h5><span class="badge badge-danger">Gasto</span></h5></td>
                        @else
                          <td><h5><span class="badge badge-danger">Transferencia</span></h5></td>
                        @endif
                        <td><p class="date">{{ $ownS->date }}</p></td>
                        <td><p>{{ $ownS->amount }} </p></td>
                      </tr>
                  @empty
                  <br>
                  <div class="empty">
                      <h4 class="container-title text-b">Datos no disponibles</h4>
                  </div>            
                  @endforelse
                </tbody>
              </table>
            </div>
    </div>
  </div>
</div>
@endsection

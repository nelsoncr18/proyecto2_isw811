@extends('layouts.app')
@section('content')
<div class="row h-75 justify-content-center align-items-center">
    <form method="POST" action="{{ route('lastyear') }}">
        @csrf
        <div class="form-group">
                <label for="id_account">Seleccione la cuenta:</label>
                <select class="form-control" required name="id_account" id="id_account">
                    @forelse ($accounts as $account)
                        <option value="{{$account->id}}">{{$account->name}}</option>
                    @empty
                        <option value="">Datos no disponibles</option>
                    @endforelse
                </select>
            </div>
        <div class="form-group">
            <button type="submit" class="btn btn-lg btn-info">Encontrar</button>
            <a class="btn btn-lg btn-danger" href="{{ route('home') }}">Cancelar</a>
        </div>
    </form>
</div>

<div class="row h-75 justify-content-center align-items-center">
    <div class="container">
  <div class="row">
    <div class="col-sm">
      <h1>Ingresos:</h1>
      <br>
      <h3>{{$currency->symbol ?? '' }} {{$totalIncome ?? ''}}</h3>
    </div>
    <div class="col-sm">
    </div>
    <div class="col-sm">
      <h1>Gastos</h1>
      <br>
      <h3>{{$currency->symbol ?? '' }} {{ $totalSpend ?? ''}}</h3>
    </div>
  </div>
</div>
@endsection
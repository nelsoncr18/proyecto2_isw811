@extends('layouts.app')
@section('content')

<header>
    <link href="{{ asset('css/home.css') }}" rel="stylesheet">
</header>
@csrf
<div class="container">    
    <h3 class="container-title">Tus cuentas</h3>
    <hr>
    <div class="card-group">
        <div class="row">
            @forelse ($accounts as $own)
                <a href="{{ route('accounts.show', $own->id) }}" class="card text-center">
                    <div class="card-header">
                        <h5>{{ $own->name }}</h5>
                    </div>
                    <div class="card-body">
                        <p class="card-text h6">{{ $own->description }}</p>
                    </div>
                    @forelse ($currencies as $curr)
                        @if($curr->id == $own->id_currency)
                            <div class="card-footer text-muted">
                                <h6>{{ $curr->name }} | Saldo: {{ $curr->symbol }} {{ $own->initial_balance }}</h6>
                            </div>
                        @endif
                    @empty    
                    @endforelse
                </a>
            &nbsp;
            @empty
                <div class="empty">
                    <h4 class="container-title">No hay datos registrados</h4>
                </div>            
            @endforelse
        </div>
    </div>
    <div class="pagination">
        {{ $accounts->links() }}      
    </div>
</div>
@endsection
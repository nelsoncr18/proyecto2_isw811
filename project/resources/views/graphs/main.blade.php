@extends('layouts.app')
@section('content')
<header>
    <link href="{{ asset('css/graph-main.css') }}" rel="stylesheet">
</header>
<div class="container">
    <div class="row">
      <div class="col-sm-9">
        <br>
        <h1 class="info text-b">Cuenta: {{ $account->name }}
          <span class="badge
            badge-success">Saldo: {{ $account->initial_balance }}</span></h1>
      </div>
    </div>
  <div class="row">
    <div class="col">
        <h1 class="text-b">Ingresos</h1>
        <div class="chart">
            {!! $chart->container() !!}
            <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset=utf-8></script>
            {!! $chart->script() !!}
        </div>
    </div>
  </div>
</div>
@endsection
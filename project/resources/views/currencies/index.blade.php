@extends('layouts.app')
@section('content')
<div class="container">
  <div class="container-fluid" >
      <div class="row">
          <div class="col-md-4">
          </div>
                <div class="container">    
                  <h3>Tus monedas</h3>
                      <div class="card-group owner">
                      <div class="row">
                        @forelse ($currenciesA as $currencie)
                      <a href="{{route('currencies.show', $currencie->id)}}" >
                        <div class="card-header">
                          <h3>{{ $currencie->symbol }}</h3> 
                       </div>
                        <div class="card-body">
                            <h3>{{ $currencie->name }}</h3>
                        </div>
                      </a>    
              &nbsp;
              @empty
              <div class="empty">
                  <h4 class="container-title">No hay datos</h4>
              </div>            
              @endforelse
                      </div>
                  </div>
                  <div class="col-md-4">    
              <a href="" class="btn-lg  btn-primary" data-toggle="modal" data-target="#createCurrencie">Agregar una moneda</a>
             </div>
              </div>
      </div>
  </div>
@endsection

<!--Action Modal -->
<div class="modal fade" id="currencieAction" tabindex="-1" role="dialog" aria-labelledby="currencieAction" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
              <h3>Elija la acción deseada</h3>  
      </div>
      <div class="modal-body">
         <a class="btn-lg btn-success"data-toggle="modal" data-dismiss="modal" data-target="#editCurrencie">Editar</a>
         <a  class="btn-lg btn-danger" data-toggle="modal" data-dismiss="modal" data-target="#deleteCurrencie">Eliminar</a>
        </div>
    </div>
  </div>
</div>

  <!-- Modal to create currencies-->
<div class="modal fade" id="createCurrencie" tabindex="-1" role="dialog" aria-labelledby="createCurrencie" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">  
        <div class="modal-body">
         <form method="POST" action="{{ route('currencies.store') }}">
            @csrf
            <div class="form-group">
                <label for="name">Nombre:</label>
                <input required type="text" required class="form-control" name="name" id="name">
            </div>
            <div class="form-group">
                <label for="symbol">Simbolo:</label>
                <input required type="text" required class="form-control" name="symbol" id="symbol">
            </div>
            <div class="form-group">
                <label for="description">Descripción:</label>
                <br>
                <textarea name="description" id="description" cols="30" rows="10"></textarea>
                <input type="hidden" name="id_user" value="{{ $user->id }}">
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-lg btn-info" value="Crear"/>
                <a class="btn btn-lg btn-danger" href="" data-dismiss="modal"> Cancelar</a>
            </div>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>


@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col">
           <form method="POST" action="{{ route('local_currencies.update',$currency->id)}}">
                  @csrf
                  @method('PUT')
           </form> 
        </div>
    <div class="col-6">
      <form method="POST" action="{{ route('currencies.update',$currency->id) }}">
              @csrf
              @method('PUT')
             <div class="form-group">
                <label for="name">Nombre:</label>
                <input required type="text" required class="form-control" name="name" id="name" value="{{$currency->name}}">
            </div>
            <div class="form-group">
                <label for="symbol">Simbolo:</label>
                <input required type="text" required class="form-control" name="symbol" id="symbol" value="{{$currency->symbol}}">
            </div>
            <div class="form-group">
                <label for="description">Descripción:</label>
                <br>
                <textarea name="description" id="description" cols="30" rows="10">{{$currency->description}}</textarea>
                <input type="hidden" name="id_user" value="{{ $currency->id }}">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-lg btn-success">Actualizar</button>
                <a href="" class="btn btn-lg btn-dark" data-toggle="modal" data-target="#deleteCurrencie">Eliminar</a>
                <a type="button" href="{{ route('currencies.index') }}"class="btn btn-lg btn-danger" >Cancelar</a> 
                </div>
          </form>
    </div>
     <div class="col">
    </div>
</div>
@endsection
 

 <!--Modal to confirm the deletion of Currencie-->
  <div class="modal fade" id="deleteCurrencie" tabindex="-1" role="dialog" aria-labelledby="deleteCurrencie" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <h3>{{$currency->name}}</h3>
        <div class="modal-header">
          
        <h5 class="modal-title" id="exampleModalLabel">{{$currency->name}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        ¿De verdad quieres eliminar esta cuenta?
        </div>
        <div class="modal-footer">
            <form action="{{ route('currencies.destroy', $currency->id) }}" method="POST">
                @csrf
                @method('DELETE')
              <button class="btn btn-success" type="submit">Confirmar</button>
              <a href="" class="btn btn-danger">Cancelar</a> 
            </form>
        </div>
      </div>
    </div>
  </div>
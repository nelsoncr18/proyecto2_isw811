@extends('layouts.app')
@section('content')
<div class="container">
  <div class="container-fluid" >
      <div class="row">
          <div class="col-md-4">
           <h3>Categorias de {{$category->name}}</h3>
          </div>
          <div class="col-md-4">
          </div>
          @if ( $category->id_user == Auth::User()->id )
              <div class="col-md-4">
              <a href="" class="btn-lg  btn-success" data-toggle="modal" data-target="#editCategory">Editar</a>
              <a href="" class="btn-lg  btn-danger" data-toggle="modal" data-target="#deleteCategory">Eliminar</a>
              <a href="" class="btn-lg  btn-primary" data-toggle="modal" data-target="#createSub">Crear categoria</a>
             </div> 
                <div class="container">  <br>
                  <div class="card-group owner">
                      <div class="row">
                             @forelse ($categories as $own)
                  <a href="{{ route('categories.show', $own->id) }}" class="card text-center">
                      <div class="card-header">
                          <h5>{{ $own->description }}</h5>
                      </div>
                  </a>
              &nbsp;
              @empty
              <div class="empty">
                  <h4 class="container-title">No hay datos disponibles</h4>
              </div>            
              @endforelse
                      </div>
                  </div>
              </div>
          @else
          @endif  
      </div>
  </div>

  <!-- Modal to create subcategories-->
  <div class="modal fade" id="createSub" tabindex="-1" role="dialog" aria-labelledby="createSub" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">  
        <div class="modal-body">
         <form method="POST" action="{{ route('categories.store') }}">
            @csrf
            <div class="form-group">
                <label for="id_type">Tipo:</label>
                <br>
                <select disabled class="form-control" name="id_type">
                    <option value = '{{ $type->id }}' >{{$type->description}}</option>
                </select>
            </div>
            <div class="form-group">
                <label for="name">Nombre:</label>
                <input required type="text" class="form-control" name="name" id="name">
            </div>
            <div class="form-group">
                <label for="description">Descripción:</label>
                <textarea cols="30" rows="10" type="text" class="form-control" name="description" id="description"></textarea>
                <input type="hidden" id="id_account" name="id_account" value="{{ $category->id_account }} ">
                <input type="hidden" id="id_parent" name="id_parent" value="{{ $category->id }} ">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-lg btn-info">Crear</button>
                <button type="button" class="btn btn-lg btn-danger"><a href="{{ route('home') }}">Cancelar</a></button>
            </div>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal to edit Categories -->
<div class="modal fade" id="editCategory" tabindex="-1" role="dialog" aria-labelledby="editCategory" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">  
        <div class="modal-body">
         <form method="POST" action="{{ route('categories.update',$category->id) }}">
              @csrf
              @method('PUT')
            <div class="form-group">
                <label for="formGroupExampleInput">Tipo:</label>
                <br>
                <select disabled class="form-control">
                    <option value = '{{ $type->id }}' >{{$type->description}}</option>
                </select>
            </div>
            <div class="form-group">
                <label for="name">Nombre:</label>
                <input required type="text" class="form-control" name="name" id="name" value="{{ $category->name }}">
            </div>
            <div class="form-group">
                <label for="description">Descripción:</label>
                <textarea cols="30" rows="10" type="text" class="form-control" name="description" id="description">{{ $category->description }}</textarea>
                <input type="hidden" name="id" value="{{ $category->id_account }} ">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-lg btn-info">Actualizar</button>
                <button type="button" class="btn btn-lg btn-danger" data-dismiss="modal">Cancelar</a></button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>


 <!--Modal to confirm the deletion of Category-->
  <div class="modal fade" id="deleteCategory" tabindex="-1" role="dialog" aria-labelledby="deleteCategory" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          
        <h5 class="modal-title" id="exampleModalLabel">{{$category->description}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        ¿De verdad quieres eliminar esta cuenta?
        </div>
        <div class="modal-footer">
            <form action="{{ route('categories.destroy', $category->id) }}" method="POST">
                @csrf
                @method('DELETE')
              <button class="btn btn-success" type="submit">Confirmar</button>
              <button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </form>
        </div>
      </div>
    </div>
  </div>
  @endsection
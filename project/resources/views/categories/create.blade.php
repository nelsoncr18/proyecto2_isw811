@extends('layouts.app')
@section('content')

<div class="row h-75 justify-content-center align-items-center">
        <form method="POST" action="{{ route('categories.store') }}">
            @csrf
            <div class="form-group">
                <input type="hidden" name="id_parent" value="">
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput">Tipo de categoria:</label>
                <select name="type_category" id="type_category">
                @foreach($categories_type as $val)
                    <option required value = '{{ $val->id }}' >{{$val->description}}</option>
                    @empty($categories_type)
                        <option value="">No hay datos disponibles</option>
                    @endempty
                @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput">Descripción:</label>
                <input type="text" class="form-control"required name="description" id="description">
                <input type="hidden" name="id_account" required value="{{ app('request')->input('id') }}">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-lg btn-info">Crear categoria</button>
            <button type="button" class="btn btn-lg btn-danger"><a href="{{route('home')}}">Cancelar</a></button>
            </div>
        </form>
    </div>
@endsection

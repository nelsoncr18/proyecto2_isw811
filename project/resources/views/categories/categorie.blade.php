@extends('layouts.app')
@section('content')

<header>
    <link href="{{ asset('css/show.css') }}" rel="stylesheet">
</header>

<div class="container">
    <div class="row">
      <div class="col-sm-9">
        <br>
        <h1 class="info text-b">{{ $account->name }}
          <span class="badge
            badge-success">Saldo Actual</span></h1>
        <h6 class="info text-b">Verde si es positivo, rojo si es negativo</h6>
      </div>
    </div>

    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{  route('accounts.show', $account->id) }}">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Library</a></li>
        <li class="breadcrumb-item"><a href="#">Data</a></li>
      </ol>
    </nav>

    <div class="row">
      <div class="col-sm-3">
        <ul class="list-group">
          <li class="list-group-item text-muted"><i class="fa fa-info-circle
              fa-1x"></i>&nbsp;&nbsp;Información</li>
          <li class="list-group-item text-right"><span class="pull-left"><strong>Moneda</strong></span>
            {{ $currency->description }}</li>
          <li class="list-group-item text-right"><span class="pull-left"><strong>Saldo inicial</strong></span> {{ $account->initial_balance }}</li>
          <li class="list-group-item text-right"><span class="pull-left"><strong>Saldo</strong></span> Calcular</li>

        </ul>
        <br>
        <ul class="list-group">
          <li class="list-group-item text-muted"><i class="fa fa-dashboard
              fa-1x"></i>&nbsp;&nbsp;Activity</li>
          <li class="list-group-item text-right"><span class="pull-left"><strong>Compartidos</strong></span>
            # shares</li>
          <li class="list-group-item text-right"><span class="pull-left"><strong>Transacciones</strong></span>
            # transacciones</li>
        </ul>
        <br>
        <ul class="list-group">
          <li class="list-group-item text-muted"><i class="fa fa-money
              fa-1x"></i>&nbsp;&nbsp;Moneda</li>
          <li class="list-group-item text-right"><span
              class="pull-left"><strong>Nombre</strong></span>
              {{ $currency->name }}</li>
          <li class="list-group-item text-right"><span
            class="pull-left"><strong>Simbolo</strong></span>
              {{ $currency->symbol }}</li>
        </ul>
      </div>

      <div class="col-sm-9">
        &nbsp;
        <ul class="nav nav-tabs">
          <li class="nav-item active"><a href="#categories" class="nav-link
              active"
              data-toggle="tab"><i class="fa fa-folder
                fa-1x"></i>&nbsp; Categorias</a></li>
          <li class="nav-item active"><a href="#transaction" class="nav-link"
              data-toggle="tab"><i class="fa fa-exchange
                fa-1x"></i>&nbsp; Transacciones</a></li>
        </ul>

        <div class="tab-content">
          <div class="tab-pane active" id="categories">
            <div class="table-responsive">
              <table class="table table-hover">
                <thead>
                  <tr class="text-b">
                    <th>Parent</th>
                    <th>Type</th>
                    <th>Description</th>
                  </tr>
                </thead>
                <tbody id="items">
                  <tr data-toggle="modal"
                    data-target="#imageModal" class="text-b">
                    <td>image des</td>
                   
                    <td>
                      <span class="badge
                        badge-success">tag</span>
                    </td>
                    <td>
                      Private
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <hr>
          </div>

          <div class="tab-pane" id="transactions">
            <br>
            <div class="container">
              <input *ngIf="userStatus" type="checkbox" checked
                data-md-icheck (change)="userState($event)" />
              <input *ngIf="!userStatus" type="checkbox"
                data-md-icheck (change)="userState($event)" />
              <span> modo incognito</span>
              &nbsp;
              <button type="submit" (click)="changeMode()"
                class="btn btn-primary">Cambiar modo</button>
            </div>
            <br>
            <div class="table-responsive">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th>Tags</th>
                  </tr>
                </thead>
                <tbody id="items">
                  <tr>
                  </tr>
                </tbody>
              </table>
            </div>
            <hr>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;
    protected $fillable = ['id_user', 'id_account', 'id_parent', 'id_type', 'name', 'description', 'active'];
}

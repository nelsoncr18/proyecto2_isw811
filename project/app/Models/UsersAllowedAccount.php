<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersAllowedAccount extends Model
{
    public $timestamps = false;
    protected $fillable = ['id', 'id_user', 'id_account'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public $timestamps = false;
    protected $fillable = ['id_owner','name','description','id_currency','initial_balance','active'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RateExchange extends Model
{
    public $timestamps = false;
    protected $fillable = ['id', 'id_user', 'id_local_currency', 'id_foreign_currency0', 'active'];
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /*
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_parent' => 'nullable|numeric',
            'id_account' => 'required|numeric',
            'id_type' => 'numeric',
            'name' => 'required|min:1',
            'description' => 'nullable',
        ];
    }
}

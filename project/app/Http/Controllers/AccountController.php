<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Account;
use App\Models\Category;
use App\Models\CategoryType;
use App\Models\Currency;
use App\Models\Category_Type;
use App\Models\Transaction;
use App\Models\UsersAllowedAccount;
use App\Http\Requests\AccountRequest;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::User();
        $currencies = Currency::where([['id_user', $user->id], ['active', true]])->get();
        return view('accounts.create', compact('currencies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::User();
        $account = new Account();
        $account->id_owner         = $user->id;
        $account->name            = request('name');
        $account->description     = request('description');
        $account->id_currency     = request('id_currency');
        $account->initial_balance = request('initial_balance');
        $account->active          = 'true';
        $account->save();

        return redirect()->route('home')
            ->with('success', 'Tu cuenta ha sido creada!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::User();
        $categories = Category::where([
            ['id_parent', null], ['id_user', $user->id], ['active', true], ['id_account', $id]
        ])->paginate(6, ['*'], 'categories');
        $currencies = Currency::where([
            ['id_user', $user->id], ['active', true]
        ])->get();
        $account = Account::where('id', $id)->firstOrFail();
        $transactions = Transaction::where([
            ['id_account', $account->id],
            ['active', true]
        ])->orderBy('date', 'desc')->paginate(6, ['*'], 'transactions'); //Transactions of selected account

        $shared = DB::table('users')
            ->join('users_allowed_accounts', 'users_allowed_accounts.id_user', '=', 'users.id')
            ->select(['users.*'])->where('users_allowed_accounts.id_account', '=', $account->id)
            ->paginate(6, ['*'], 'shared');

        $currency = Currency::where('id', $account->id_currency)->firstOrFail();
        $categories_type = CategoryType::get();
        return view(
            'accounts.show',
            [
                'account' => $account, 'currencies' => $currencies, 'currency' => $currency, 'categories' => $categories,
                'categories_type' => $categories_type, 'shared' => $shared, 'transactions' => $transactions
            ]
        );
    }






    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AccountRequest $request, $id)
    {
        $account = Account::where('id', $id)->firstOrFail();
        $account->name            = $request->input('name');
        $account->description     = $request->input('description');
        $account->id_currency     = $request->input('id_currency');
        $account->initial_balance = $request->input('initial_balance');
        $account->save();
        return redirect()->route('accounts.show', $id)
            ->with('success', 'Tu cuenta ha sido actualizada!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account = Account::where('id', $id)->firstOrFail();
        $account->delete();
        return redirect()->route('home')
            ->with('warning', 'Tu cuenta ha sido eliminada!');
    }
}

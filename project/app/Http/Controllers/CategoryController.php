<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\CategoryType;
use App\Models\Category;
use App\Http\Requests\CategoryRequest;
use Auth;

class CategoryController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::get();
        $categories_type = CategoryType::get();
        return view('categories.create', compact('categories'), compact('categories_type'));
    }

     /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $user = Auth::User();
        $category = new Category();
        $category->name = $request->input('name');
        $category->description = $request->input('description');
        $category->id_parent = $request->input('id_parent') ?? null;
        $category->id_account = $request->input('id_account') ?? null;
        if ($request->input('id_parent')) {
            $cat = Category::where('id', $request->input('id_parent'))->first();
            $category->id_type = $cat->id_type;
        } else {
            $category->id_type = $request->input('id_type');
        }
        $category->active = 'true';
        $category->id_user = $user->id;
        $category->save();
        if ($request->id_parent) {
            return redirect()->route('categories.show', $request->id_parent)
                ->with('warning', 'La subcategoria ha sido creada con exito!');
        } else {
            return redirect()->route('accounts.show', $request->id_account)
                ->with('warning', 'La categoria ha sido creada con exito!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::where('id', $id)->firstOrFail();
        $type = CategoryType::where('id', $category->id_type)->first();
        $categories_type = CategoryType::get();
        $user = Auth::User();
        $categories = Category::where([
            ['id_parent', $category->id],
            ['id_user', $user->id],
            ['active', true]
        ])->get();
        return view('categories.show', compact('categories', 'category', 'categories_type', 'type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::where('id', $id)->firstOrFail();
        $category->update($request->all());
        $category->save();
        return redirect('accounts/' . $category->id_account);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::where('id', $id)->firstOrFail();
        $category->active = false;
        $category->update();
        return redirect('home')->with('message', 'Your account was deleted');
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\Account;
use App\Models\TransactionType;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;

class TransactionController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = Auth::User();
        $categoriesI = Category::where([['id_user', $user->id], ['id_type', 1], ['active', true]])->get();
        $categoriesS = Category::where([['id_user', $user->id], ['id_type', 2], ['active', true]])->get();
        $account = Account::where('id', $request->input('idac'))->first();
        $transactions_type = TransactionType::get();
        $accounts = Account::where([['id_owner', $user->id], ['id', '!=', $request->input('idac')]])->get();
        return view('transactions.create', compact('accounts', 'transactions_type', 'account', 'categoriesI', 'categoriesS'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $account                    = Account::where('id', $request->account)->first();
        $current_timestamp          = date('d-m-Y');
        $transaction                = new Transaction();
        $transaction->id_type       = request('transaction_type');
        $transaction->id_account     = request('account');
        if (request('categoryI')) {
            $transaction->id_category   = request('categoryI');
        }else {
            $transaction->id_category   = request('categoryS');
        }
        $transaction->amount        = $current_timestamp;
        $transaction->amount        = request('amount');
        $transaction->detail        = $request->input('description');

        if ($request->transaction_type == 1 && $account) {
            $transaction->save();
            DB::table('accounts')->where('accounts.id', $request->account)->increment('initial_balance', $request->amount);
            return redirect('accounts/' . $request->account)->with('success', 'Tu transaccion se realizo!');
        } else if ($request->transaction_type == 2 && $account) {
            if (request('amount') < $account->initial_balance) {
                $transaction->save();
                DB::table('accounts')->where('accounts.id', $request->account)->decrement('initial_balance', $request->amount);
                return redirect('accounts/' . $request->account)->with('success', 'Tu transaccion se realizo!');
            } else {
                return back()->with('error', 'El monto de la transacción es mayor que el saldo permitido!');
            }
        } else {
            $transaction2               = new Transaction();
            $transaction2->id_type      = 1;
            $transaction2->id_account   = request('accountO');
            $transaction2->id_category  = request('category');
            $transaction2->amount       = $current_timestamp;
            $transaction2->amount       = request('amount');
            $transaction2->detail       = $request->input('description');
            if (request('amount') < $account->initial_balance) {
                $transaction->id_type = 2;
                $transaction->save();
                $transaction2->save();
                DB::table('accounts')->where('accounts.id', $request->accountO)->increment('initial_balance', $request->amount);
                DB::table('accounts')->where('accounts.id', $request->account)->decrement('initial_balance', $request->amount);
                return redirect('accounts/' . $request->account)->with('success', 'Tu transaccion se realizo!');
            } else {
                return back()->with('error', 'El monto de la transacción es mayor que el saldo permitido!');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::User();
        $transactions_type = TransactionType::get();
        $transaction = Transaction::where('id', $id)->firstOrFail();
        $accounts = Account::where('id_owner', $user->id)->get();
        return view('transactions.show', compact('transaction', 'accounts', 'transactions_type'));
    }

      /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transaction = Transaction::where('id', $id)->firstOrFail();
        $transaction->update($request->all());
        $transaction->save();
        return redirect('home')->with('success', 'Tu transaccion se realizo!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = Transaction::where('id', $id)->firstOrFail();
        $transaction->active = false;
        $transaction->update();
        return redirect('home')->with('success', 'Your transaction was deleted!');
    }
}
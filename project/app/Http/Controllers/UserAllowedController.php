<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\UsersAllowedAccount;
use App\Models\Account;
use App\User;

class UserAllowedController extends Controller{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    { }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::where('email', strtolower($request->input('email')))->first();
        if ($user) {
            $id_allowed = UsersAllowedAccount::where([['id_user', $user->id], ['id_account', $request->input('id_account')]])->first();
            if ($id_allowed) {
                return redirect()->route('accounts.show', $request->input('id_account'))
                    ->with('info', 'Este usuario ya esta en uso');
            }
            $user_allowed = new UsersAllowedAccount();
            $user_allowed->id_user    = $user->id;
            $user_allowed->id_account = $request->input('id_account');
            $user_allowed->save();
            return redirect()->route('accounts.show', $request->input('id_account'))
                ->with('success', 'Tu cuenta fue compartida con ' . $user->email . '!');
        }
        return redirect()->route('accounts.show', $request->input('id_account'))
            ->with('error', 'Este usuario no existe!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $revoke = UsersAllowedAccount::where([['id_user', $request->input('id_user')], ['id_account', $id]])->first();
        $user = User::where('id', $request->input('id_user'))->first();
        $revoke->delete();
        return redirect()->route('accounts.show', $id)
            ->with('info', 'The access for ' . $user->email . ' was revoked');
    }
}
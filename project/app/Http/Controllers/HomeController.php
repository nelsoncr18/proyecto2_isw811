<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Account;
use App\Models\Currency;
use App\Models\UserAllowed;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::User();
        $accounts = Account::where('id_owner', $user->id)->paginate(9);
        $currencies = Currency::where('id_user', Auth::User()->id)->get();
        return view('home', compact('accounts', 'currencies'));
    }

    public function shareds()
    {
        $user = Auth::User();
        $shared = DB::table('users_allowed_accounts')
            ->join('accounts', 'accounts.id', '=', 'users_allowed_accounts.id_account')
            ->select(['accounts.*'])->where('users_allowed_accounts.id_user', '=', $user->id)
            ->paginate(9);
        return view('accounts.shared', compact('shared', $shared));
    }
}

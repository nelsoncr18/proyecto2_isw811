<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Account;
use App\Models\Transaction;
use App\Models\Currency;
use Carbon\Carbon;

class ReportsController extends Controller{
       //Method to redirect to between view
       public function between_two_dates(){
        $user = Auth::User();
        $accounts = Account::where('id_owner', $user->id)->get();
        return view('reports.between', compact('accounts'));
    }

      //Method to redirect to last_mont view
      public function lastmonth(){
        $user = Auth::User();
        $accounts = Account::where('id_owner', $user->id)->get();
        return view('reports.last_month', compact('accounts'));
    }

       //Method to redirect to last_mont view
       public function lastyear(){
        $user = Auth::User();
        $accounts = Account::where('id_owner', $user->id)->get();
        return view('reports.last_year', compact('accounts'));
    }

    //Method to load transactions made it between 2 dates
    public function loadBetween(Request $request){
        $user = Auth::User();
        $totalIncome=0;
        $totalSpend=0;
        $currency = new Currency();
        $account = new Account();

        $accounts = Account::where('id_owner', $user->id)->get();

        $transactions=Transaction::where([
            ["date",">=",$request->init],
            ["date","<=",$request->end],
            ['id_account',$request->id_account]
            ])->get();

        $transactionsS=Transaction::where([
                ["date",">=",$request->init],
                ["date","<=",$request->end],
                ['id_account',$request->id_account],
                ['id_type',2]
                ])->orderBy('date', 'desc')->paginate(6, ['*'], 'transactionsS');

        $transactionsI=Transaction::where([
            ["date",">=",$request->init],
            ["date","<=",$request->end],
            ['id_account',$request->id_account],
            ['id_type',1]
            ])->orderBy('date', 'desc')->paginate(6, ['*'], 'transactionsI');

        if ($transactions) {
            foreach ($transactions as $transaction) {
                if ($transaction->id_type == 1) {
                    $totalIncome= $totalIncome+$transaction->amount;
                }
                if ($transaction->id_type == 2 || $transaction->id_type == 3) {
                    $totalSpend= $totalSpend+$transaction->amount;
                } 
            }

            $account = Account::where('id', $request->id_account)->first();
            $currency = Currency::where('id', $account->id_currency)->first();
        }
        return view('reports.between', compact('totalIncome','totalSpend','accounts', 'currency','transactionsS','transactionsI'));
    }

    //Method to load transactions made it into selected month
    public function search_by_month(Request $request){
        $currency = new Currency();
        $account = new Account();
        $user = Auth::User();
        $currentdate  = Carbon::now();
        $previousDate = Carbon::now()->subDays(28);
        $accounts = Account::where('id_owner', $user->id)->get();
        $totalIncome=0;
        $totalSpend=0;

         $account = Account::where('id', $request->id_account)->first();
        $currency = Currency::where('id', $account->id_currency)->first();
        
        $transactions=Transaction::where([['id_account',$request->id_account]])
        ->whereBetween('date', [$previousDate, $currentdate])
        ->get();

        $transactionsS=Transaction::where([['id_account',$request->id_account],['id_type',2]])
        ->whereBetween('date', [$previousDate, $currentdate])->orderBy('date', 'desc')->get();

        $transactionsI=Transaction::where([['id_account',$request->id_account],['id_type',1]])
        ->whereBetween('date', [$previousDate, $currentdate])->orderBy('date', 'desc')->get();

        if ($transactions) {
            foreach ($transactions as $transaction) {
                if ($transaction->id_type == 1) {
                    $totalIncome= $totalIncome+$transaction->amount;
                }
                if ($transaction->id_type == 2 || $transaction->id_type == 3) {
                    $totalSpend= $totalSpend+$transaction->amount;
                } 
            }
        }
        return view('reports.last_month', compact('totalIncome','totalSpend','accounts','currency', 'transactionsS','transactionsI'));
    }

    //Method to load transactions made it into selected month
    public function search_by_year(Request $request){
        $currency = new Currency();
        $account = new Account();
        $user = Auth::User();
        $currentdate  = Carbon::now();
        $previousDate = Carbon::now()->subDays(365);
        $accounts = Account::where('id_owner', $user->id)->get();
        $totalIncome=0;
        $totalSpend=0;

        $account = Account::where('id', $request->id_account)->first();
        $currency = Currency::where('id', $account->id_currency)->first();
        
        $transactions=Transaction::where([['id_account',$request->id_account]])
        ->whereBetween('date', [$previousDate, $currentdate])
        ->get();
        if ($transactions) {
            foreach ($transactions as $transaction) {
                if ($transaction->id_type == 1) {
                    $totalIncome= $totalIncome+$transaction->amount;
                }
                if ($transaction->id_type == 2 || $transaction->id_type == 3) {
                    $totalSpend= $totalSpend+$transaction->amount;
                } 
            }
        }else {
            return view('reports.last_year', compact('totalIncome','totalSpend','accounts','currency'));    
        }
            return view('reports.last_year', compact('totalIncome','totalSpend','accounts','currency'));
    }


}
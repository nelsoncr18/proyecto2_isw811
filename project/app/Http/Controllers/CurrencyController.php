<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Currency;
use App\Models\Account;
use Auth;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::User();
        $account = Account::where('id_owner', $user->id);
        $currenciesA = Currency::where([
            ['id_user', $user->id],
            ['active',true]
            ])->get();
        return view('currencies.index',compact('currenciesA','account','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::User();
        $currency = new Currency();
        $currency->name             = request('name');
        $currency->symbol           = request('symbol');
        $currency->description      = request('description');
        $currency->id_user          = $user->id;
        $currency->active           = 'true';
        $currency->save();
        
        return redirect('currencies')->with('message', 'Su cuenta a sido creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $currency = Currency::where('id', $id)->firstOrFail();
        return view('currencies.show',compact('currency'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currency = Currency::where('id', $id)->firstOrFail();
        $currency->update($request->all());
        $currency->save();
        return redirect('currencies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currency = Currency::where('id', $id)->firstOrFail();
        $currency->active = false;
        $currency->update();
        return redirect('currencies');
    }

 /*   public function setcurrency($id){
        dd($id);
    }
*/
}
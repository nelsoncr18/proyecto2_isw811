<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Models\Account;
use App\Models\Transaction;
use App\Models\CategoryType;
use App\Models\Category;
use \App\Charts\GraphAccounts;

class GraphController extends Controller{

    public $borderColors = [
        "rgba(255, 99, 132, 1.0)",
        "rgba(22,160,133, 1.0)",
        "rgba(255, 205, 86, 1.0)",
        "rgba(51,105,232, 1.0)",
        "rgba(244,67,54, 1.0)",
        "rgba(34,198,246, 1.0)",
        "rgba(153, 102, 255, 1.0)",
        "rgba(255, 159, 64, 1.0)",
        "rgba(233,30,99, 1.0)",
        "rgba(205,220,57, 1.0)"
    ];

    public $fillColors = [
        "rgba(255, 99, 132, 0.2)",
        "rgba(22,160,133, 0.2)",
        "rgba(255, 205, 86, 0.2)",
        "rgba(51,105,232, 0.2)",
        "rgba(244,67,54, 0.2)",
        "rgba(34,198,246, 0.2)",
        "rgba(153, 102, 255, 0.2)",
        "rgba(255, 159, 64, 0.2)",
        "rgba(233,30,99, 0.2)",
        "rgba(205,220,57, 0.2)"

    ];

    public function index()
    {
        
    }

    public function show(int $id){
        $user = Auth::User();
        $account = Account::where([['id_owner', $user->id],['id',$id],['active', true]])->firstOrFail();
        $categories = Category::where([['id_user', $user->id], ['active', true],])->get();
        $transactionsI = Transaction::where([['id_type', 1], ['id_account', $account->id], ['active', true]])->get();
        $transactionsS = Transaction::where([['id_type', 2], ['id_account', $account->id], ['active', true]])->get();
        $categoriesI = Category::where([['id_user', $user->id], ['id_type', 1]], ['active', true])->get();
        $categoriesS = Category::where([['id_user', $user->id], ['id_type', 2]], ['active', true])->get();
        
        $names = [];
        
        //$names = $categories->map(function ($category) {
        //    return collect($category->toArray())
        //        ->only('name')->all();
        //});
        
        foreach ($categories as $category) {
            array_push($names, $category->name);
        }

        //doughnut
        
        $chart = new GraphAccounts;
        $chart->labels($names);
        $chart->dataset('Transacciones por categoria', 'doughnut', [10, 25, 13])
            ->color($this->borderColors)
            ->backgroundcolor($this->fillColors);

        $chart2 = new GraphAccounts;
        $chart2->labels(['Oct', 'Nov', 'Dic']);
        $chart2->dataset('Users by trimester', 'pie', [10, 25, 13]);

        return view('graphs.main', compact('chart', 'chart2', 'account'));
    }
}
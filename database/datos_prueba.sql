--------------------------------users--------------------------------------------------------------
INSERT INTO public.users(
	id, name, email, email_verified_at, password, remember_token, created_at, updated_at)
	VALUES (1, 'johnny', 'johnnymendez730@gmail.com', null, '12345678', null, null, null);
	
INSERT INTO public.users(
	id, name, email, email_verified_at, password, remember_token, created_at, updated_at)
	VALUES (2, 'carlos', 'carlos123@gmail.com', null, '12345678', null, null, null);
	
INSERT INTO public.users(
	id, name, email, email_verified_at, password, remember_token, created_at, updated_at)
	VALUES (3, 'nelson', 'nelson123@gmail.com', null, '12345678', null, null, null);
	
INSERT INTO public.users(
	id, name, email, email_verified_at, password, remember_token, created_at, updated_at)
	VALUES (4, 'pablo', 'pablo@gmail.com', null, '12345678', null, null, null);
	
INSERT INTO public.users(
	id, name, email, email_verified_at, password, remember_token, created_at, updated_at)
	VALUES (5, 'misael', 'misael123@gmail.com', null, '12345678', null, null, null);

--------------------------user-allow-account------------------------------------------------------------
INSERT INTO public.users_allowed_accounts(
	id, id_user, id_account)
	VALUES (1, 1, 1);
	
INSERT INTO public.users_allowed_accounts(
	id, id_user, id_account)
	VALUES (2, 2, 2);

INSERT INTO public.users_allowed_accounts(
	id, id_user, id_account)
	VALUES (3, 3, 3);
	
INSERT INTO public.users_allowed_accounts(
	id, id_user, id_account)
	VALUES (4, 4, 4);
	
INSERT INTO public.users_allowed_accounts(
	id, id_user, id_account)
	VALUES (5, 5, 5);

------------------------------rate change------------------------------------------------------------------
INSERT INTO public.rate_exchange(
	id, id_user, id_local_currency, id_foreign_currency, active)
	VALUES (1, 1, 1, 1, true);
	
	INSERT INTO public.rate_exchange(
	id, id_user, id_local_currency, id_foreign_currency, active)
	VALUES (2, 2, 2, 2, true);
	
	INSERT INTO public.rate_exchange(
	id, id_user, id_local_currency, id_foreign_currency, active)
	VALUES (3, 3, 3, 3, true);
	
	INSERT INTO public.rate_exchange(
	id, id_user, id_local_currency, id_foreign_currency, active)
	VALUES (4, 4, 4, 4, true);
	
	INSERT INTO public.rate_exchange(
	id, id_user, id_local_currency, id_foreign_currency, active)
	VALUES (5, 5, 5, 5, true);

INSERT INTO public.currencies(
	name, symbol, description, id_user, active)
	VALUES ('euro', '€', 'euro', 1, true);

INSERT INTO public.categories(
	id, id_user, id_account, id_parent, id_type, name, description, active)
	VALUES (1, 1, 1, 1, 1, 'Salario', 'Pago diciembre', true);

INSERT INTO public.apps(
	id, description, active)
	VALUES 1, 'App', true);

INSERT INTO public.category_types(
	id, description)
	VALUES (1, 'Dinero');

INSERT INTO public.transactions(
	id, id_type, id_account, id_category, date, detail, amount, active)
	VALUES (1, 1, 1, 1, '11-11-2020', 'Pago disney plus', 1000, true);

INSERT INTO public.transaction_types(
	id, description)
	VALUES (1, 'Gasto');
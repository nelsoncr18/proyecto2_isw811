CREATE TABLE apps (
    id SERIAL PRIMARY KEY NOT NULL,
    description TEXT NOT NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE transaction_types (
    id SERIAL PRIMARY KEY NOT NULL,
    description TEXT NOT NULL UNIQUE
);

CREATE TABLE category_types (
    id SERIAL PRIMARY KEY NOT NULL,
    description TEXT NOT NULL UNIQUE
);

CREATE TABLE users (
    id SERIAL PRIMARY KEY NOT NULL, -- Generado por bd
    ex_id TEXT UNIQUE NOT NULL, -- Tomado del api
    id_app INT NOT NULL, -- FK A Tabla apps
    username TEXT UNIQUE, -- Tomado del usuario
    name TEXT NOT NULL, -- Tomado del api
    lastname TEXT NOT NULL, -- Tomado del api
    email TEXT UNIQUE NOT NULL, -- Tomado del api
    password TEXT NOT NULL -- Tomado del usuario
);

CREATE TABLE currencies (
    id SERIAL PRIMARY KEY NOT NULL,
    name TEXT NOT NULL UNIQUE,
    symbol TEXT NOT NULL,
    description TEXT NOT NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE rate_exchange (
    id SERIAL PRIMARY KEY NOT NULL,
    id_user INT NOT NULL,
    id_local_currency INT NOT NULL,
    id_foreign_currency INT NOT NULL,
    active BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE accounts (
    id SERIAL PRIMARY KEY NOT NULL,
    id_owner INT NOT NULL,
    name TEXT NOT NULL,
    description TEXT NOT NULL,
    id_currency INT NOT NULL,
    initial_balance FLOAT NOT NULL,
    icon TEXT,
    active BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE users_allowed_account (
    id SERIAL PRIMARY KEY NOT NULL,
    id_user INT NOT NULL,
    id_account INT NOT NULL
);

CREATE TABLE categories (
    id SERIAL PRIMARY KEY NOT NULL,
    id_user INT NOT NULL,
    id_account INT NOT NULL,
    id_parent INT,
    id_type INT NOT NULL,
    description TEXT NOT NULL,
    icon TEXT,
    active BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE transactions (
    id SERIAL PRIMARY KEY NOT NULL,
    id_type INT NOT NULL,
    id_account INT NOT NULL,
    date timestamp without time zone,
    detail TEXT NOT NULL,
    amount FLOAT NOT NULL
);

ALTER TABLE users
    ADD CONSTRAINT id_app_user FOREIGN KEY (id_app) REFERENCES apps (id) ON
    UPDATE
        CASCADE;

ALTER TABLE rate_exchange
    ADD CONSTRAINT rate_currency_users FOREIGN KEY (id_user) REFERENCES users (id) ON
    UPDATE
        CASCADE;

ALTER TABLE rate_exchange
    ADD CONSTRAINT id_currency_local FOREIGN KEY (id_local_currency) REFERENCES currencies (id) ON
    UPDATE
        CASCADE;

ALTER TABLE rate_exchange
    ADD CONSTRAINT id_currency_foreign FOREIGN KEY (id_foreign_currency) REFERENCES currencies (id) ON
    UPDATE
        CASCADE;

ALTER TABLE accounts
    ADD CONSTRAINT id_owner_account FOREIGN KEY (id_owner) REFERENCES users (id) ON
    UPDATE
        CASCADE;

ALTER TABLE accounts
    ADD CONSTRAINT id_currency_account FOREIGN KEY (id_currency) REFERENCES currencies (id) ON
    UPDATE
        CASCADE;

ALTER TABLE users_allowed_account
    ADD CONSTRAINT id_user_allowed FOREIGN KEY (id_user) REFERENCES users (id) ON
    UPDATE
        CASCADE;

ALTER TABLE users_allowed_account
    ADD CONSTRAINT id_account_allowed FOREIGN KEY (id_account) REFERENCES accounts (id) ON
    UPDATE
        CASCADE;

ALTER TABLE categories
    ADD CONSTRAINT id_user_category FOREIGN KEY (id_user) REFERENCES users (id) ON
    UPDATE
        CASCADE;

ALTER TABLE categories
    ADD CONSTRAINT id_parent_category FOREIGN KEY (id_parent) REFERENCES categories (id) ON
    UPDATE
        CASCADE;
        
ALTER TABLE categories
    ADD CONSTRAINT id_account_category FOREIGN KEY (id_account) REFERENCES accounts (id) ON
    UPDATE
        CASCADE;

ALTER TABLE categories
    ADD CONSTRAINT id_type_category FOREIGN KEY (id_type) REFERENCES category_types (id) ON
    UPDATE
        CASCADE;

ALTER TABLE transactions
    ADD CONSTRAINT id_type_trasasction FOREIGN KEY (id_type) REFERENCES transaction_types (id) ON
    UPDATE
        CASCADE;

ALTER TABLE transactions
    ADD CONSTRAINT id_account_transaction FOREIGN KEY (id_account) REFERENCES accounts (id) ON
    UPDATE
        CASCADE;

